class Template {
  constructor(message = 'hello world') {
    this._message = message
  }

  get message() {
    return this._message
  }
}

class TemplateFabric extends Template {
  static async createTemplateAsync() {
    // example: dataFromDb = await getDataFromDb
    // example: new Template(dataFromDb)
    return new TemplateFabric()
  }
}

module.exports = TemplateFabric
