const Template = require('../src/Template')

it('test tests', async () => {
  const template = await Template.createTemplateAsync()
  expect(template.message).toEqual('hello world')
})
