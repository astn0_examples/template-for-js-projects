FROM node:current
WORKDIR /usr/src/app
EXPOSE 4000
COPY . /usr/src/app/
RUN npm install
CMD ["npm", "start"]